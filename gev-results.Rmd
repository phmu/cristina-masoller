# Results of the extreme value analysis

## Interpretation of the fitting results

After splitting each time series into annual blocks and extracting
just the maximal or minimal values the generalized extreme value (GEV)
distribution will be fitted to all those extreme events. Its
parameters do have the following meanings.

$$\begin{equation}
  G(z) = \exp \left\{ - \left[ 1 + \xi \left( \frac{z - \mu}{\sigma}
  \right) \right]^{-\frac{1}{\xi}} \right\} \text{, } \xi \neq 0.
  (\#eq:gev-cdf)
\end{equation}$$

### Location $\mu$

This parameter can be considered the average extreme event occurring
once in the provided time scale (on an annual basis in our
analysis). 

The higher the location parameter, the stronger do the extreme events
deviate from zero.

### Scale $\sigma$

The scale parameter can be interpreted as the variance of the GEV
distribution. 

The higher its value, the stronger do the extremes fluctuate in their
values. 

### Shape $\xi$

The shape does describe the tail of the PDF of a process. In case the
annual maximum values were extracted, it describes the right-hand side
of the PDF and for the annual minima the left-hand side.

For values bigger then 0 the GEV distributions is of *Frechet*-type and
has an unbound power law tail. For $\xi=0$ the distribution
simplifies to the *Gumbel* one and has an exponential tail. Finally,
for values smaller than 0 the distribution is of *Weibull*-type has an
upper (or lower, in case of the block minima) endpoint.

The value itself has to be bigger than -1 to ensure the existence of
the maximum likelihood estimator used during the fitting procedure and
only for values larger than -0.5 the regularity conditions are
fulfilled. For shape parameters larger then 1 the moments of the
distribution doesn't exist anymore. The usual values encountered in
climate are in the interval between -0.5 and 0.5.

Therefore, areas holding a positive and large value of the shape
parameter are bound to manifest more extreme events.

## Temperature anomalies

To start with, let's have a look at the right and the left tail of the
PDF of the temperatures anomalies.

### Left tail

```{r 'temp-min', fig.width = 12, fig.height = 18, echo = FALSE}
load( "data/gev-results.RData" )

## Plot the results
pp.1 <- plot.parameter.ggplot(
    temperature.anomalies.gev.parameters.min, "location",
    plot.title = "Location parameter of left tail (temp. anom.)",
    limits = limits.temperature$location )
pp.2 <- plot.parameter.ggplot(
    temperature.anomalies.gev.parameters.min, "scale",
    plot.title = "Scale parameter of left tail (temp. anom.)",
    limits = limits.temperature$scale )
pp.3 <- plot.parameter.ggplot(
    temperature.anomalies.gev.parameters.min, "shape",
    plot.title = "Shape parameter of left tail (temp. anom.)",
    limits = limits.temperature$shape )
multiplot( list( pp.1, pp.2, pp.3 ) )
 
```

The **location** and **scale** parameter yield a consistent
picture. The deviations from the temperatures anomalies do happen in
the polar regions and especially on land. The latter finding is quite
intuitive since the ocean serves as a reservoir of heat and thus
compresses the variability.

The **shape** parameter, on the other hand, does show a rather random
pattern with predominantly negative values. But one can find regions
of positive shape parameters close to the polar region on the southern
hemisphere, in central and western Europe, at the west coast of the
USA and Mexico, and more frequently on land outside the polar region. 

This means there are regions and counties, like France, for which the
lower end of the PDF features a power law tail and temperature
significantly lower than the average anomaly are way more likely to
observe than in other parts of the globe.

### Right tail

```{r 'temp-max', fig.width = 12, fig.height = 18, echo = FALSE}
load( "data/gev-results.RData" )

## Plot the results
pp.1 <- plot.parameter.ggplot(
    temperature.anomalies.gev.parameters.max, "location",
    plot.title = "Location parameter of right tail (temp. anom.)",
    limits = limits.temperature$location )
pp.2 <- plot.parameter.ggplot(
    temperature.anomalies.gev.parameters.max, "scale",
    plot.title = "Scale parameter of right tail (temp. anom.)",
    limits = limits.temperature$scale )
pp.3 <- plot.parameter.ggplot(
    temperature.anomalies.gev.parameters.max, "shape",
    plot.title = "Shape parameter of right tail (temp. anom.)",
    limits = limits.temperature$shape )
multiplot( list( pp.1, pp.2, pp.3 ) )
 
```

The right-hand side of the PDF yields a picture qualitatively similar
to the one the left-hand side.

The **scale** parameters indicates that the centers of high variations
shift more to the poles when looking a events exceeding the
temperature anomalies. Especially in Europe the variation decreases a
lot while an increase does happen in the eastern part of Russia.

The **shape** seems to display an even more random behavior in
general but also a large region of positive values in the ocean west
of southern America. This area does indeed corresponds to the part of
the globe which is most strongly manifesting the impact of the el niño
southern oscillation and thus features the biggest deviations from the
temperature anomalies.

## Instantaneous frequencies (finite differences)

### Left tail

```{r 'freq-min', fig.width = 12, fig.height = 18, echo = FALSE}
load( "data/gev-results.RData" )

## Plot the results
pp.direct.1 <- plot.parameter.ggplot(
    instantaneous.frequencies.min.direct, "location",
    plot.title = "Location parameter of left tail (frequencies)",
    limits = limits.frequency.direct$location )
pp.direct.2 <- plot.parameter.ggplot(
    instantaneous.frequencies.min.direct, "scale",
    plot.title = "Scale parameter of left tail (frequencies)",
    limits = limits.frequency.direct$scale )
pp.direct.3 <- plot.parameter.ggplot(
    instantaneous.frequencies.min.direct, "shape",
    plot.title = "Shape parameter of left tail (frequencies)",
    limits = limits.frequency.direct$shape )
multiplot( list( pp.direct.1, pp.direct.2, pp.direct.3 ) )

```

See below.

### Right tail

```{r 'freq-max', fig.width = 12, fig.height = 18, echo = FALSE}
load( "data/gev-results.RData" )

## Plot the results
pp.direct.1 <- plot.parameter.ggplot(
    instantaneous.frequencies.max.direct, "location",
    plot.title = "Location parameter of right tail (frequencies)",
    limits = limits.frequency.direct$location )
pp.direct.2 <- plot.parameter.ggplot(
    instantaneous.frequencies.max.direct, "scale",
    plot.title = "Scale parameter of right tail (frequencies)",
    limits = limits.frequency.direct$scale )
pp.direct.3 <- plot.parameter.ggplot(
    instantaneous.frequencies.max.direct, "shape",
    plot.title = "Shape parameter of right tail (frequencies)",
    limits = limits.frequency.direct$shape )
multiplot( list( pp.direct.1, pp.direct.2, pp.direct.3 ) )

```

The distribution of the instantaneous frequencies is remarkably
symmetric in all three GEV parameters. Apart from minor exceptions the
PDF of the frequencies at one site is either bounded (negative
**shape** values) at both ends or exhibits a power law (positive
**shape** values) or exponential (**shape** values around zero) decay
in both directions. I did not expected this since the three different
PDFs in Fig. 6 (d), (e), and (f) of your 2016 *Entropy* paper
suggested to have a certain asymmetry in regions of higher
variance. But I tend to consider the results plausible. As a zeroth
order approximation I would model the drivers of the climate at each
individual site with a (symmetric) noise term. If there is a strong
variability in the climate I don't see why it should just influence
the right side of the PDF. But I'm by no means educated in the field
of climate science.

The **location** and the **scale** parameter feature the almost
identical behavior as the mean and standard deviation in the paper
*Global Atmospheric Dynamics Investigated by Using Hilbert Frequency
Analysis*. If we would model the frequencies as a stochastic process,
this would indicate a strong stability since I am not looking at the
mean and the variance of the whole series but at the average and
variance of the annual maxima or minima of the frequency (it's not the
exactly the average and variance but the location and scale parameters
of the GEV distribution are very similar to them).

The **shape** parameter is the most interesting one. It shows that
regions of lesser variance (and thus smaller average annual maxima or
minima) show a power law decay in both sides of their PDFs. Areas with
higher variance, on the other hand, tend to be have bounded tails in
their PDFs.

Therefore the regions of higher variance will have larger return
levels (values) at smaller return periods (quantiles). E.g. when
looking at the average frequency occurring once per 6 months or 3
years, the regions of higher variances would yield higher values. But
when handling far more large quantiles, like the frequency only
occurring once every 100 years, the power law tail of the PDF of the
regions with lesser variance kicks in the values in these regions are
the higher then all the others.

### Return levels

```{r 'return-level-max', fig.width = 12, fig.height = 12, echo = FALSE}
load( "data/gev-results-return-levels.RData" )

plot.return.level.ggplot(
    freq.max,
    plot.title = "Return level of the frequencies (right tail)" )

```

```{r 'return-level-min', fig.width = 12, fig.height = 12, echo = FALSE}
load( "data/gev-results-return-levels.RData" )

plot.return.level.ggplot(
    freq.min,
    plot.title = "Return level of the frequencies (left tail)" )

```

## Smooth derivatives

For the sake of completeness I also add the results obtained using a
natural spline to smooth the phases series in order to calculate their
individual derivatives (the frequency series).

The resulting patterns differ from the ones obtained using finite
differences method. The borders are much more sharp, the regions of
lower variances are extended, and a strong asymmetry in the PDF is
introduced. Apart from this, also numerical artifacts are obtained
when fitting the right side of the PDF (violet points, only present in
the map of the shape parameter). Especially due to the last
observation the finite difference method might be the more appropriate
one. 

### Left tail
 
```{r 'freq-min-spline', fig.width = 12, fig.height = 18, echo = FALSE}
load( "data/gev-results.RData" )

## Plot the results
pp.spline.1 <- plot.parameter.ggplot(
    instantaneous.frequencies.min.spline, "location",
    plot.title = "Location parameter of right tail (frequencies)",
    limits = limits.frequency.spline$location )
pp.spline.2 <- plot.parameter.ggplot(
    instantaneous.frequencies.min.spline, "scale",
    plot.title = "Scale parameter of right tail (frequencies)",
    limits = limits.frequency.spline$scale )
pp.spline.3 <- plot.parameter.ggplot(
    instantaneous.frequencies.min.spline, "shape",
    plot.title = "Shape parameter of right tail (frequencies)",
    limits = limits.frequency.spline$shape )
multiplot( list( pp.spline.1, pp.spline.2, pp.spline.3 ),
          tt.title = "Smoothed derivatives" )
```

### Right tail
 
```{r 'freq-max-spline', fig.width = 12, fig.height = 18, echo = FALSE}
load( "data/gev-results.RData" )

## Plot the results
pp.spline.1 <- plot.parameter.ggplot(
    instantaneous.frequencies.max.spline, "location",
    plot.title = "Location parameter of right tail (frequencies)",
    limits = limits.frequency.spline$location )
pp.spline.2 <- plot.parameter.ggplot(
    instantaneous.frequencies.max.spline, "scale",
    plot.title = "Scale parameter of right tail (frequencies)",
    limits = limits.frequency.spline$scale )
pp.spline.3 <- plot.parameter.ggplot(
    instantaneous.frequencies.max.spline, "shape",
    plot.title = "Shape parameter of right tail (frequencies)",
    limits = limits.frequency.spline$shape )
multiplot( list( pp.spline.1, pp.spline.2, pp.spline.3 ),
          tt.title = "Smoothed derivatives" )
```
