## In case there are some functions I would have to define multiple times.


## Plot an interactive map using the leaflet package.
plot.shape.parameter.leaflet <- function( gev.results, legend.title = "" ){
  plot.data <- tibble(
      longitude = gev.results[ , 2 ],
      latitude = gev.results[ , 1 ],
      location = gev.results[ , 3 ],
      scale = gev.results[ , 4 ],
      shape = gev.results[ , 5 ] )
  ## Concatenate all information about one point into a popup
  plot.data <- mutate(
      plot.data,
      popup = paste0( "longitude: ", round( longitude, 2 ), "<br/>",
                     "latitude: ", round( latitude, 2 ), "<br/>",
                     "location: ", round( location, 2 ), "<br/>",
                     "scale: ", round( scale, 2 ), "<br/>",
                     "shape: ", round( shape, 2 ), "<br/>" ) )

  ## Generating a color palette for the shape parameters.
  color.max.shape <- max( plot.data$shape )
  color.min.shape <- min( plot.data$shape )
  palette.shape <- colorNumeric(
      c( RColorBrewer::brewer.pal( 6, "Spectral" ) ),
      c( color.min.shape, color.max.shape ) )

  ## Generating the actual map object
  map.leaflet <- leaflet( data = plot.data,
                         width = "100%", height = "1400" ) %>%
    addTiles( "http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png",
             attribution = "<code> Kartendaten: \uA9 <a href='https://openstreetmap.org/copyright'>OpenStreetMap</a>-Mitwirkende, SRTM | Kartendarstellung: \uA9 <a href='http://opentopomap.org'>OpenTopoMap</a> (<a href='https://creativecommons.org/licenses/by-sa/3.0/'>CC-BY-SA</a> </code>)",
             group = "OpenTopoMaps" ) %>%
    addTiles( group = "OpenStreetMaps" ) %>%
    addLayersControl(
        baseGroups = c( "OpenTopoMaps", "OpenStreetMaps" ),
        overlayGroups = "stations",
        options = layersControlOptions( collapsed = FALSE ) ) %>%
    addHeatmap(lng = ~longitude, lat = ~latitude, intensity = ~shape ) %>%
    addLegend( position = "topright", title = legend.title,
              pal = palette.shape,
              values = c( color.max.shape, color.min.shape ) )
  map.leaflet
  
  return( map.leaflet )
}

## Plot a map using the ggplot2 package.
plot.parameter.ggplot <- function( gev.results,
                                  parameter = c( "location",
                                                "scale", "shape" ),
                                  plot.title = "", diff = FALSE,
                                  limits = c( NA, NA ) ){
  plot.data <- tibble(
      longitude = gev.results[ , 2 ],
      latitude = gev.results[ , 1 ],
      location = gev.results[ , 3 ],
      scale = gev.results[ , 4 ],
      shape = gev.results[ , 5 ] )
  ## Concatenate all information about one point into a popup
  plot.data <- mutate(
      plot.data,
      popup = paste0( "longitude: ", round( longitude, 2 ), "<br/>",
                     "latitude: ", round( latitude, 2 ), "<br/>",
                     "location: ", round( location, 2 ), "<br/>",
                     "scale: ", round( scale, 2 ), "<br/>",
                     "shape: ", round( shape, 2 ), "<br/>" ) )

  ## Center the map around zero degree
  plot.data$longitude[ plot.data$longitude >= 180 ] <- 
    plot.data$longitude[ plot.data$longitude >= 180 ] - 360

  ## Discard all results, which will be most probably artifacts
  plot.data$shape[ plot.data$shape <= -.9 ] <- NA

  pp.1 <- ggplot( data = plot.data, 
                 aes_string( "longitude", "latitude",
                            fill = parameter ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap() + ggtitle( plot.title )
  if ( diff ){
    pp.1 <- pp.1 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits,
                           midpoint = 0, na.value = "darkorchid" )
  } else {
    pp.1 <- pp.1 +
    scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                         mid = "yellow", limits = limits,
                         midpoint = 0, na.value = "darkorchid" )
  }
  return( pp.1 )
}

## Plot a map using the ggplot2 package.
plot.return.level.ggplot <- function( plot.data, limits = c( NA, NA ),
                                     plot.title = "",
                                     type = c( "max", "min" ) ){
  if ( missing( type ) ){
    type <- "max"
  }
  ## Center the map around zero degree
  plot.data$longitude[ plot.data$longitude >= 180 ] <- 
    plot.data$longitude[ plot.data$longitude >= 180 ] - 360

  ## If the return level of the minima are desired, they will have
  ## negative values and the logarithmic rescaling will fail. In this
  ## case both the limits and the return levels will be multiplied by
  ## minus one and just their values in the legend will be printed as
  ## negative values.
  if ( type == "min" ){
    limits <- limits* -1
    plot.data$return.level.1 <- plot.data$return.level.1 * -1
    plot.data$return.level.2 <- plot.data$return.level.2 * -1
  }
  
  ## Plot of the first return level.
  pp.1 <- ggplot( data = plot.data, aes( x = longitude, y = latitude,
                                        fill = return.level.1 ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap()

  if ( type == "max" ){
    pp.1.breaks <- c( limits[ 1 ]* 1.1,
                     mean( log( max( limits ) - min( limits ) ) ),
                     limits[ 2 ]* .9 )
    pp.1.labels <- as.character( round( pp.1.breaks, 1 ) )
    pp.1 <- pp.1 +
      scale_fill_distiller( palette = "YlOrRd", direction = 1,
                           breaks = pp.1.breaks, labels = pp.1.labels,
                           trans = "log", limits = limits,
                           guide = guide_colourbar( title = "5 years" ),
                           na.value = "darkorchid" )
  } else {
    pp.1.breaks <- c( limits[ 2 ]* 1.1,
                     mean( log( max( limits ) - min( limits ) ) ),
                     limits[ 1 ]* .9 )
    pp.1.labels <- paste0( "-", round( pp.1.breaks, 1 ) )
    pp.1 <- pp.1 +
      scale_fill_distiller( palette = "YlGnBu", direction = 1,
                           breaks = pp.1.breaks,
                           labels = pp.1.labels,
                           trans = "log", limits = rev( limits ),
                           guide = guide_colourbar( title = "5 years" ),
                           na.value = "darkorchid" )
  }
  ## Plot of the second one
  pp.2 <- ggplot( data = plot.data, aes( x = longitude, y = latitude,
                                        fill = return.level.2 ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap()
  
  if ( type == "max" ){
    pp.2 <- pp.2 +
      scale_fill_distiller( palette = "YlOrRd", direction = 1,
                           breaks = pp.1.breaks, labels = pp.1.labels,
                           trans = "log", limits = limits,
                           guide = guide_colourbar( title = "100 years" ),
                           na.value = "darkorchid" )
  } else {
   
    pp.2 <- pp.2 +
      scale_fill_distiller( palette = "YlGnBu", direction = 1,
                           breaks = pp.1.breaks, labels = pp.1.labels,
                           trans = "log", limits = rev( limits ),
                           guide = guide_colourbar( title = "100 years" ),
                           na.value = "darkorchid" )
  }
  multiplot( list( pp.1, pp.2 ), tt.title = plot.title )
  invisible()
}


## Plotting the difference between the return levels of two windows
plot.return.level.diff.ggplot <- function( plot.data, limits = c( NA, NA ),
                                     plot.title = "",
                                     type = c( "max", "min" ) ){
  if ( missing( type ) ){
    type <- "max"
  }
  ## Center the map around zero degree
  plot.data$longitude[ plot.data$longitude >= 180 ] <- 
    plot.data$longitude[ plot.data$longitude >= 180 ] - 360
  ## If the return level of the minima are desired, they will have
  ## negative values and the logarithmic rescaling will fail. In this
  ## case both the limits and the return levels will be multiplied by
  ## minus one and just their values in the legend will be printed as
  ## negative values.
  if ( type == "min" ){
    limits <- limits* -1
    plot.data$return.level.1 <- plot.data$return.level.1 * -1
    plot.data$return.level.2 <- plot.data$return.level.2 * -1
  }
  ## Plot of the first return level.
  pp.1 <- ggplot( data = plot.data, aes( x = longitude, y = latitude,
                                        fill = return.level.1 ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap()
  if ( type == "max" ){
    pp.1.breaks <- c( limits[ 1 ]* 1.1,
                     mean( log( max( limits ) - min( limits ) ) ),
                     limits[ 2 ]* .9 )
    pp.1.labels <- as.character( round( pp.1.breaks, 1 ) )
    pp.1 <- pp.1 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits,
                           guide = guide_colourbar( title = "5 years" ),
                           midpoint = 0, na.value = "darkorchid" )
  } else {
    pp.1.breaks <- c( limits[ 2 ]* 1.1,
                     mean( log( max( limits ) - min( limits ) ) ),
                     limits[ 1 ]* .9 )
    pp.1.labels <- paste0( "-", round( pp.1.breaks, 1 ) )
    pp.1 <- pp.1 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits,
                           guide = guide_colourbar( title = "5 years" ),
                           midpoint = 0, na.value = "darkorchid" )
  }
  ## Plot of the second one
  pp.2 <- ggplot( data = plot.data, aes( x = longitude, y = latitude,
                                        fill = return.level.2 ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap()
  if ( type == "max" ){
    pp.2 <- pp.2 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits,
                           guide = guide_colourbar( title = "100 years" ),
                           midpoint = 0, na.value = "darkorchid" )
  } else {
    pp.2 <- pp.2 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits,
                           guide = guide_colourbar( title = "100 years" ),
                           midpoint = 0, na.value = "darkorchid" )
  }
  multiplot( list( pp.1, pp.2 ), tt.title = plot.title )
  invisible()
}


## Plot a map using the ggplot2 package.
plot.moments.ggplot <- function( freq.data, moment.data,
                                limits = c( NA, NA ),
                                plot.title = "", diff = FALSE ){
  ## The data containing the moments does not provide information
  ## about the corresponding longitude or latitude. They have to be
  ## extracted from a object containing information about the
  ## instantaneous frequency.
  plot.data <- data.frame(
      longitude = freq.data[ , 2 ],
      latitude = freq.data[ , 1 ],
      mean = moment.data$mean,
      var = moment.data$var,
      skewness = moment.data$skewness,
      kurtosis = moment.data$kurtosis )
  ## Center the map around zero degree
  plot.data$longitude[ plot.data$longitude >= 180 ] <- 
    plot.data$longitude[ plot.data$longitude >= 180 ] - 360
  ## Plot of the mean
  pp.1 <- ggplot( data = plot.data, aes( x = longitude, y = latitude,
                                        fill = mean ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap()
  if ( diff ){
    pp.1 <- pp.1 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits$mean,
                           midpoint = 0, na.value = "darkorchid" )
  } else {
    pp.1 <- pp.1 +
    scale_fill_distiller( palette = "RdYlBu", direction = -1,
                          limits = limits$mean,
                         na.value = "darkorchid" )
  }
  ## Plot of the variance
  pp.2 <- ggplot( data = plot.data, aes( x = longitude, y = latitude,
                                        fill = var ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap()
  if ( diff ){
    pp.2 <- pp.2 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits$var,
                           midpoint = 0, na.value = "darkorchid" )
  } else {
    pp.2 <- pp.2 +
      scale_fill_distiller( palette = "YlOrRd", direction = 1,
                           limits = limits$var,
                           na.value = "darkorchid" )
  }
  ## Plot of the skewness
  pp.3 <- ggplot( data = plot.data, aes( x = longitude, y = latitude,
                                        fill = skewness ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap()
  if ( diff ){
    pp.3 <- pp.3 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits$skewness,
                           midpoint = 0, na.value = "darkorchid" )
  } else {
    pp.3 <- pp.3 +
    scale_fill_distiller( palette = "RdYlBu", direction = -1,
                         limits = limits$skewness,
                         na.value = "darkorchid" )
  }
  ## Plot of the kurtosis
  pp.4 <- ggplot( data = plot.data, aes( x = longitude, y = latitude,
                                        fill = kurtosis ) ) +
    geom_raster( interpolate = TRUE ) + theme_bw() +
    borders( "world" ) + coord_quickmap()
  if ( diff ){
    pp.4 <- pp.4 +
      scale_fill_gradient2( low = "deepskyblue3", high = "red3",
                           mid = "white", limits = limits$kurtosis,
                           midpoint = 0, na.value = "darkorchid" )
  } else {
    pp.4 <- pp.4 +
    scale_fill_distiller( palette = "YlOrRd", direction = 1,
                         limits = limits$kurtosis,
                         na.value = "darkorchid" )
  }
  multiplot( list( pp.1, pp.2, pp.3, pp.4 ), tt.title = plot.title )
  invisible()
}
