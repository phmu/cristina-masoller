Performing the extreme value analysis on Hilbert phases extracted from
grid data.

The actual analysis is done in the *.Rmd files and the resulting
document can be accessed via this
[link](http://phmu.io.pks.mpg.de/cristina-masoller).

Per default the scripts will only produce the graphics using the data
obtained in previous calculations (to save same time in the deployment
step). To trigger the **recalculation** of all data instead, just
remove the *data/rerun* folder.

# [documentation](documentation/)
This folder contains all the documentation provided by Cristina and
all the mail correspondence.

# [data](data/)
Contains the grid data of the Hilbert phases, some example series of
the data, and the intermediate results used to generate the plots and
graphics.

In the subfolder *data/rerun* all the temporary files produced while
evaluating the R chunks will be saved.
