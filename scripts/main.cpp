#include <cstddef>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>


void RewriteSeries(const std::string & input_filename, const std::string & output_filename)
{
  std::ifstream inFile(input_filename.data(), std::ios::binary | std::ios::in);
  std::ofstream outFile(output_filename.data());
  if (!inFile.is_open())
    std::cout << "Couldn't read from file \"" << input_filename << "\"." << std::endl;
  else
    {
      std::cout << "Reading time series from file \"" << input_filename << "\"..." << std::endl;
      size_t N, T;
      inFile.read(reinterpret_cast<char *>(&N), sizeof(size_t));
      inFile.read(reinterpret_cast<char *>(&T), sizeof(size_t));
      outFile << N << '\n' << T;
        
      for (long int i = 0; i < N; ++i)
        {
	  double lat, lon;
	  std::vector<double> TimeSeries(T);
            
	  //std::cout << "\rnode " << i+1 << " of " << N; std::cout.flush();
	  inFile.read(reinterpret_cast<char *>(&lat), sizeof(double));
	  inFile.read(reinterpret_cast<char *>(&lon), sizeof(double));
	  inFile.read(reinterpret_cast<char *>(TimeSeries.data()), T * sizeof(double));
	  outFile << '\n' << lat << '\t' << lon;
	  for (double term: TimeSeries)
	    outFile << '\t' << term;
        }
      inFile.close();
      outFile.close();
      std::cout << std::endl << "Done." << std::endl << std::endl;
    }
}

int main(int argc, char** argv)
{
  if (argc == 3)
    {
      RewriteSeries(argv[1], argv[2]);
      return 0;
    }
  else
    {
      std::cout << "Error: this program needs 2 parameters (input filename and output filename)." << std::endl;
      return 1;
    }
}
