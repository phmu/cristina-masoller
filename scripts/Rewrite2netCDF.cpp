/* A port to NetCDF of the script Dario wrote for loading and
   converting the temperature anomalies and instantaneous frequencies
   of the ERAINTERIM data set.
*/
#include <cstddef>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <netcdf>

// Error code, which will be returned in case something went wrong in
// writing the NetCDF file.
static const int NC_ERR = 2;

// The origin of the POSIX time, January 1st 1970, will be used. From
// that day on (corresponding to the value of 0) all following days
// will be referenced by an integer value indicating the number of
// days to the origin.
// Since the data starts at December 4th 1980, a constant offset will
// be defined to describe the first date in the series.
static const int TIME_OFFSET = 3990;

// Number of elements in the longitude or latitude vector as
// well as their offsets and increments.
static const int LENGTH_LONGITUDE = 144;
static const int LENGTH_LATITUDE = 73;
static const float LONGITUDE_OFFSET = 0.0;
static const float LONGITUDE_INCREMENT = 2.5;
static const float LATITUDE_OFFSET = 90;
static const float LATITUDE_INCREMENT = -90;

RewriteSeries(const std::string & input_filename, const std::string & output_filename)
{
  std::ifstream inFile(input_filename.data(), std::ios::binary | std::ios::in);
  std::ofstream outFile(output_filename.data());
  if (!inFile.is_open())
    std::cout << "Couldn't read from file \"" << input_filename << "\"." << std::endl;
  else {
    // Getting the dimension of the file
    std::cout << "Reading time series from file \"" << input_filename << "\"..." << std::endl;
    size_t N, T;
    inFile.read(reinterpret_cast<char *>(&N), sizeof(size_t));
    inFile.read(reinterpret_cast<char *>(&T), sizeof(size_t));
    outFile << N << '\n' << T;

    try {
      // Open a NetCDF file to store the data in.
      netCDF::NcFile dataFile( "erainterim_anomalies_instantaneous-frequencies.nc",
			       netCDF::NcFile::replace );

      if( !dataFile.is_valid() ){
	std::cout << "Can't open the NetCDF file." << std::endl;
	return NC_ERR;
      }
     
      // Define the dimensions of the NetCDF file.
      netCDF::NcDim longitudeDim  = dataFile.add_dim( "longitude",
						      LENGTH_LONGITUDE );
      netCDF::NcDim latitudeDim  = dataFile.add_dim( "latitude",
						     LENGTH_LATITUDE );
      netCDF::NcDim timeDim  = dataFile.add_dim( "time", T );

      // Define the variables stored in the NetCDF file. These will
      // be the vectors of the individual variables defining the
      // grid of the data.
      int times[ T ];
      float longitudes[ N ];
      float latitudes[ N ];

      // The time spans a period from the 4 December 1980 to the 28
      // July 2015.
      for ( int dd = 0; dd < T; dd++ ){
	times[ dd ] = TIME_OFFSET + dd;
      }
      for ( int ll = 0; ll < LENGTH_LONGITUDE; ll++ ){
	longitudes[ ll ] = LONGITUDE_OFFSET + ll * LONGITUDE_INCREMENT;
      }
      for ( int ll = 0; ll < LENGTH_LATITUDE; ll++ ){
	latitudes[ ll ] = LATITUDE_OFFSET + ll * LATITUDE_INCREMENT;
      }
	
      netCDF::NcVar *longitudeVector, *timeVector, *latitudeVector;
      if ( !( timeVar = dataFile.addVar( "time", netCDF::ncInt, timeDim ) ) ){
	std::cout << "Error while writing the time coordinate" << std::endl;
      }
      if ( !( latitudeVar = dataFile.addVar( "longitude", netCDF::ncDouble, longitudeDim ) ) ){
	std::cout << "Error while writing the longitude coordinate" << std::endl;
      }
      if ( !( latitudeVar = dataFile.addVar( "latitude", netCDF::ncDouble, latitudeDim ) ) ){
	std::cout << "Error while writing the site coordinate" << std::endl;
      }

      // Define the variables, which will contain the actual data
      netCDF::NcVar *tempAnomaliesVar, *instantaneousFrequenciesVar;
      if ( !( tempAnomaliesVar = dataFile.add_var( "temperature anomalies",
						   netCDF::ncFloat,
						   longitudeDim, latitudeDim, timeDim ) ) ){
	std::cout << "Error while defining the temperature anomalies" << std::endl;
	return NC_ERR;
      }
      if ( !( instantaneousFrequenciesVar = dataFile.add_var( "instantaneous frequencies",
							      netCDF::ncFloat, longitudeDim,
							      latitudeDim, timeDim ) ) ){
	std::cout << "Error while defining the instantaneous frequencies" << std::endl;
	return NC_ERR;
      }

      // Adding the attributes describing the data.
      if ( !longitudeVar->add_att( "units", "degree east" ) ){
	std::cout << "Error writing the attributes of the longitudes" << std::endl;
	return NC_ERR;
      }
      if ( !latitudeVar->add_att( "units", "degree north" ) ){
	std::cout << "Error writing the attributes of the latitudes" << std::endl;
	return NC_ERR;
      }
      if ( !timeVar->add_att( "units", "days" ) ){
	std::cout << "Error writing the attributes of the time" << std::endl;
	return NC_ERR;
      }
      if ( !tempAnomaliesVar->add_att( "units", "degree celsius" ) ){
	std::cout << "Error writing the attributes of the temperature data" << std::endl;
	return NC_ERR;
      }

      // Write the variable data.
      if ( !longitudeVar->put( longitudes, longitudeDim ) ){
	std::cout << "Error while writing the longitudes" << endl;
	return NC_ERR;
      }
      if ( !latitude->put( latitudes, latitudeDim ) ){
	std::cout << "Error while writing the latitude" << endl;
	return NC_ERR;
      }
      if ( !timeVar->put( times, timeDim ) ){
	std::cout << "Error while writing the times" << endl;
	return NC_ERR;
      }
      // Write the data to the file.
      data.putVar( dataOut );
	
      for (long int ii = 0; ii < N; ++ii){
	  double lat, lon;
	  std::vector<double> TimeSeries(T);
            
	  std::cout << "\nwriting series number " << ii + 1 << " of " << N; std::cout.flush();
	  inFile.read(reinterpret_cast<char *>(&lat), sizeof(double));
	  inFile.read(reinterpret_cast<char *>(&lon), sizeof(double));
	  inFile.read(reinterpret_cast<char *>(TimeSeries.data()), T * sizeof(double));

	  if ( !tempAnomaliesVar->put( TimeSeries, lon, lat ) ){
	    std::cout << "Error while writing the temperature anomalies" << endl;
	    return NC_ERR;
	  }
	}
      inFile.close();
      outFile.close();
      std::cout << std::endl << "Done." << std::endl << std::endl;
    } catch( netCDF::exceptions::NcException& e ){
      e.what();
      return NC_ERR;
    }
  }
}

/* Since the geographical information about a time series is encoded
   separately in the specific rows, this function will extract the
   relation between the station ID and the position of the series
   prior to the conversion into NetCDF. The station ID is the number
   of occurrence of the time series in the .dat file.
*/
void GetSeriesPositions( const std::string & input_filename ){

  std::ifstream inFile(input_filename.data(), std::ios::binary | std::ios::in);
  // File the meta information will be stored in.
  std::ofstream outFile( "era_meta_information.dat" )
  if (!inFile.is_open())
    std::cout << "Couldn't read from file \"" << input_filename << "\"." << std::endl;
  else {
    // Getting the dimension of the file
    std::cout << "Reading dimensions from file \"" << input_filename << "\"..." << std::endl;
    size_t N, T;
    inFile.read(reinterpret_cast<char *>(&N), sizeof(size_t));
    inFile.read(reinterpret_cast<char *>(&T), sizeof(size_t));
    outFile << N << '\n' << T;

    int SeriesID[ N ];
    double longitudes[ N ];
    double latitudes[ N ];

    // Loop over all the different time series to store the meta
    // information about the series in the variables defined above.
    for (long int ii = 0; ii < N; ++ii){
      double lat, lon;
      std::vector<double> TimeSeries(T);
            
      //std::cout << "\rnode " << i+1 << " of " << N; std::cout.flush();
      inFile.read(reinterpret_cast<char *>(&lat), sizeof(double));
      inFile.read(reinterpret_cast<char *>(&lon), sizeof(double));
      inFile.read(reinterpret_cast<char *>(TimeSeries.data()), T * sizeof(double));

      SeriesID[ ii ] = ii;
      longitudes[ ii ] = lon;
      latitudes[ ii ] = lat;
    }
    inFile.close();
    std::cout << std::endl << "Done." << std::endl << std::endl;
  }
  return 0;
}

int main(int argc, char** argv)
{
  if (argc == 3)
    {
      RewriteSeries(argv[1], argv[2]);
      return 0;
    }
  else
    {
      std::cout << "Error: this program needs 2 parameters (input filename and output filename)." << std::endl;
      return 1;
    }
}
