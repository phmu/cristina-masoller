for FILE in "$@"; do

gnuplot << EOT

set terminal pdf color fname "Helvetica" fsize 12

#STATISTICAL CALCULATIONS
stats '$FILE' u 1:3 nooutput #To get the max and min value
min = STATS_min_y
max = STATS_max_y
#END OF STATISTICAL CALCULATIONS
#min = 0.000737374
#max = 0.607823


set output "${FILE%".dat"}.pdf"
set multiplot

set palette model RGB maxcolors 0
set palette defined (0 'white', 1 'blue', 2 'green', 3 'yellow', 4 'orange', 5 'red')
set lmargin at screen 0.1
set rmargin at screen 0.84
set bmargin at screen 0.12
set tmargin at screen 0.93
set xrange [0:360]
set yrange [-90:90]
set cbrange [min:max]
set ytics -90,30,90
set xtics 0,90,270

plot "$FILE" u 2:1:3 notitle w image pixels
unset xtics
unset ytics
plot '/Users/dario/Dropbox/dottorato/programmazione/mydata/coste.dat' w l lw 2 lc rgb 'black' notitle

#set print "control-values.txt"
#print min,max

EOT

done
