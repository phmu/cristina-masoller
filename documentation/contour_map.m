%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  contour_map.m
%  Función para graficar mapas
%
%  Input: x - eje de longitudes
%         y - eje de latitudes
%         f - matriz de datos Dim(longitud,latitud)
%         ci - intervalo de contornos
%         lev - niveles de contornos. Si lev se da como argumento, entonces no usa ci.
%
%  Ejemplo: contour_map(X,Y,squeeze(SST(1,:,:)),0,(0:.2:2))
%           grafica un mapa con el primer valor temporal de SST, usando niveles de 
%           contorno de 0 a 2 con un paso de 0.2.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function contour_map(x,y,f,ci,lev)

%Transpone matriz de datos
f=f';

%Encuentra límite de valores de los ejes x, y
latmin = min(y);   
latmax = max(y);    
lonmin = min(x);  
lonmax = max(x); 

[nlat nlon] = size(f);   %Calcula tamaño de la matriz de datos f.
latp = y;        
lonp = x;        

%Carga archivo con la costa de los continentes y lo grafica
load coast
lat2=[lat; lat];
long2=[long; long+360];
plot(long2,lat2,'k','linewidth',1)     
axis([lonmin lonmax round(latmin) round(latmax)]);
grid on

hold on

%Define contornos para graficar si el numero de argumentos de entrada es 4.
minf = min(min(f));
maxf = max(max(f));
if (ci == 0)
    ci = 0.1*(maxf-minf);   % Gives 9 contours
end


if nargin == 5      %Si el número de argumentos es 5, ejecuta estos comandos. Si no, ejecuta los de mas abajo.

    [c,v]=contourf(lonp,latp,f,lev);     %grafica contornos llenos
    %clabel(c,v);                         %le pone los valores a los contornos
    caxis([min(lev) max(lev)])           %ajusta los niveles de los colores
    plot(long2,lat2,'k','linewidth',2)   %grafica nuevamente las costas 

else

  if(minf >= 0)   % Not an anomaly plot - all contours solid
    contlevs = [minf : ci : maxf ];
    [c,v]=contour(lonp,latp,f,contlevs);      %grafica contornos 
    clabel(c,v);
  elseif (maxf <= 0)
    contpos = 0.5*(1:2:19)*ci;
    [c,v]=contour(lonp,latp,f,-contpos,'b--');   % Negative contours dashed
    clabel(c,v); 
  else            % Anomaly plot
    contpos = 0.5*(1:2:19)*ci;
    if(any(contpos<max(max(f))))
     [c,v]=contour(lonp,latp,f,contpos,'r-');     % Positive contours solid
     clabel(c,v);
    end
    if(any(-contpos>min(min(f))))     
     [c,v]=contour(lonp,latp,f,-contpos,'b--');   % Negative contours dashed
     clabel(c,v); 
    end
  end

end

hold off

